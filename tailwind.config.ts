import type { Config } from "tailwindcss";

export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  darkMode: "selector",
  theme: {
    extend: {
      colors: {
        primary: "rgb(var(--color-primary))",
      },
    },
  },
  variants: {
    extend: {
      dark: {
        colors: {
          primary: "#000000",
        },
      },
    },
  },
  plugins: [],
};
